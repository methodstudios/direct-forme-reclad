<?php

/**
 * @author Eugene Eastlake
 * Sends email query.
 */
 

function DoSpamCheck($value) {
  
  $spam_attempt = 0;
  
  // What's not allowed in the input?
  $arr_forbidden = array( "\r", "\n", "%0a", "%0d", "Content-Type:", "bcc:","to:","cc:" ); 
  
  // Go over input to see if any input field holds dangerous code
  foreach ( $arr_forbidden as $forbidden ){
    if ( substr_count ( $value, $forbidden ) > 0 ) {
		
		// If forbidden input found flag spam attempt
		$spam_attempt++;
	}
  }
  return $spam_attempt;
  
} 


// Now check the POST array that holds all fields posted by the form
$die = 0;
foreach( $_POST as $value ){
	  // For each field do the spam check!
      if( DoSpamCheck($value) > 0  ){
		
		// If spam input found set flag to exit script
		$die++;
	  
	  }
}


if( $die > 0 ){
		// Set the message to be shown when spam attempt detected
		$die_message = "<strong>{$_SERVER['REMOTE_ADDR']} Is blocked because Spam input is detected!</strong>\n<br />\n";
		
		// Terminate the script in case of spam
		die($die_message);

}


if(isset($_POST['email'])) {
	
	$fname      = stripslashes(strip_tags(htmlspecialchars($_POST['fname'])));
	$lname      = stripslashes(strip_tags(htmlspecialchars($_POST['lname'])));
	$email      = stripslashes(strip_tags(htmlspecialchars($_POST['email'])));
	$phone      = stripslashes(strip_tags(htmlspecialchars($_POST['phone'])));
	$body       = stripslashes(strip_tags(htmlspecialchars($_POST['message'])));
	
	$recipient  = "chris@formereclad.co.nz";
	$subject    = "Message from Forme website";
	
	$headers    = "Content-Type: text/html; charset=\"windows-1251\"\r\n";
	$headers   .= "MIME-Version: 1.0 \r\n";
	$headers   .= "From: ".$email. "\r\n";
	
	$message    = "You have a message from ". $fname ." ". $lname ."</br>Phone: " .$phone."</br></br>";
	$message   .= $body;
	
	// Send Email
	if (mail($recipient, $subject, $message, $headers)) {
		header( 'Location: ../../form_thanks.html' ) ;
	}else{
		echo("<strong>There was a problem sending your email, please try again.</strong>");
	}
	
}

?>